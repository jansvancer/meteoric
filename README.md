# Meteoric #

Jednoduchá aplikace pro zobrazení seznamu spadnutých meteoritů na zemi od roku 2011.

### Instalace ###

Projekt využívá knihoven třetích tran, které je potřeba nainstalovat pomocí nástroje CocoaPods.

Použité knihovny:

- Realm: databáze pro uložení stažených záznamů z API
- Alamofire: knihovna pro síťovou komunikaci
- SwiftyJSON: knihovna pro snažší parsování json struktury

### Použité nástroje ###

- Implementační jazyk: Swift 4
- CocoaPods

### Poznámky ###

Projekt je to dosti malý a tak nevím úplně co na tom vymyslet. Vyzkoušel jsem si alespoň pro mě novou práci s Realm databází, jako alternativou ke CoreData s kterou jsem pracoval dříve.

Navíc nejsem grafik, ale i tak jsem se pokusil o aspoň trošku vzhlédné UI.