//
//  AppDelegate.swift
//  Meteoric
//
//  Created by Jan Švancer on 06.11.17.
//  Copyright © 2017 Jan Švancer. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // setup navigation bar
        let navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationBarAppearance.shadowImage = UIImage()
        navigationBarAppearance.barTintColor = Style.darkColor
        navigationBarAppearance.tintColor = Style.lightColor
        navigationBarAppearance.titleTextAttributes = [NSAttributedStringKey.foregroundColor: Style.lightColor]
        navigationBarAppearance.isTranslucent = false
        UIApplication.shared.statusBarStyle = .lightContent
        
        // download data once a day
        self.updateData()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    /**
     *  Updates data once a day
     *  Function check last sync date saved in UserDefaults and determines when to download new data from API
     */
    func updateData() {
        var update = false
        
        let lastSyncKey = "lastSync"
        let userDefaults = UserDefaults.standard
        
        // *********** TEST *************** //
        //userDefaults.set(Date(timeInterval: -24*60*60, since: Date()), forKey: lastSyncKey)
        //userDefaults.synchronize()
        
        if let lastSync = userDefaults.object(forKey: lastSyncKey) as? Date {
            // check last sync
            let calendar = NSCalendar.current
            let dateComponent = calendar.dateComponents([.day], from: lastSync, to: Date())
            if let dayDifference = dateComponent.day {
                update = dayDifference >= 1
            }
        } else {
            // first time - download data
            update = true
        }
        
        if update {
            APIService.shared.downloadData { (meteorites) in
                guard let meteorites = meteorites else {
                    print("Data download failed")
                    return
                }
                
                print("New data downloaded")
                DBService.shared.storeMeteoritesInBackground(meteorites: meteorites)
                
                // save last sync date
                userDefaults.set(Date(), forKey: lastSyncKey)
                userDefaults.synchronize()
            }
        }
    }
    
}

