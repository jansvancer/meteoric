//
//  Style.swift
//  Meteoric
//
//  Created by Jan Švancer on 07.11.17.
//  Copyright © 2017 Jan Švancer. All rights reserved.
//

import UIKit

struct Style {
    static var tintColor = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)
    static var lightTintColor = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 0.75)
    static var darkColor = UIColor(red: 55/255.0, green: 51/255.0, blue: 52/255.0, alpha: 1.0)
    static var lightColor = UIColor(red: 234/255.0, green: 234/255.0, blue: 234/255.0, alpha: 1.0)
    
}
