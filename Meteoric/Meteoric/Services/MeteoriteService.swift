//
//  MeteoriteService.swift
//  Meteoric
//
//  Created by Jan Švancer on 06.11.17.
//  Copyright © 2017 Jan Švancer. All rights reserved.
//

import Foundation
import SwiftyJSON

class MeteoriteService {
    
    // Singleton
    static let shared: MeteoriteService = {
        let instance = MeteoriteService()
        
        return instance
    }()
    
    /**
     *  Translate json values from API into meteorite objects array
     *
     *  @param  jsonValues  received JSON data
     *  @return             array of meteorite objects
     */
    func translateMeteoriteData(jsonValues: [[String: Any]]) -> [Meteorite] {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        var meteorites = [Meteorite]()
        
        let json = JSON(jsonValues)
        
        for (_, value) in json {
            let meteorite = Meteorite()
            meteorite.id = value["id"].intValue
            meteorite.nametype = value["nametype"].stringValue
            meteorite.fall = value["fall"].stringValue
            meteorite.mass = value["mass"].intValue
            meteorite.name = value["name"].stringValue
            meteorite.recclass = value["recclass"].stringValue
            meteorite.reclat = value["reclat"].doubleValue
            meteorite.reclong = value["reclong"].doubleValue
            
            let yearString = value["year"].stringValue
            meteorite.year = dateFormatter.date(from: yearString)
            
            meteorites.append(meteorite)
        }
        
        return meteorites
    }
    
}
