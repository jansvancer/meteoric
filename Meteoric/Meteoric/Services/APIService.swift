//
//  APIService.swift
//  Meteoric
//
//  Created by Jan Švancer on 06.11.17.
//  Copyright © 2017 Jan Švancer. All rights reserved.
//

import Foundation
import Alamofire

class APIService {
    
    let URL = "https://data.nasa.gov/resource/y77d-th95.json"
    let headers: HTTPHeaders = [
        "X-App-Token": "huMOp1g6UIKWzqtRnmx8xzJC0",
        "Accept": "application/json"
    ]
    let startYear = 2011
    
    
    // Singleton
    static let shared: APIService = {
        let instance = APIService()
        
        return instance
    }()
    
    /**
     *  Downloading date from API
     *
     *  @param  completion  completion block with downloaded data
     */
    func downloadData(completion: @escaping ([Meteorite]?) -> ()) {
        let parameters: Parameters = ["$where": "year >= '\(self.startYear)-01-01T00:00:00.000'"]
        
        Alamofire.request(self.URL, parameters: parameters, headers: self.headers).validate().responseJSON { (response) in
         
            guard response.result.isSuccess else {
                print("Error while downloading data")
                completion(nil)
                return
            }
            
            guard let jsonValues = response.result.value as? [[String: Any]] else {
                print("Cannot read received data")
                completion(nil)
                return
            }
            
            let meteorites = MeteoriteService.shared.translateMeteoriteData(jsonValues: jsonValues)
            
            completion(meteorites)
        }
    }
    
}
