//
//  DBService.swift
//  Meteoric
//
//  Created by Jan Švancer on 06.11.17.
//  Copyright © 2017 Jan Švancer. All rights reserved.
//

import Foundation
import RealmSwift

class DBService {
    
    var realm: Realm!
    
    // Singleton
    static let shared: DBService = {
        let instance = DBService()
        instance.realm = try! Realm()
        
        return instance
    }()
    
    /**
     *  Helping method for deleting all data in Realm DB
     */
    func cleanDB() {
        
        try! self.realm.write {
            self.realm.deleteAll()
        }
    }
    
    /**
     *  Helping method to clean meteorites table
     */
    func cleanMeteorites() {
        
        try! self.realm.write {
            let allMeteorites = self.realm.objects(Meteorite.self)
            self.realm.delete(allMeteorites)
        }
    }
    
    /**
     *  Store meteorites to DB on background thread
     *
     *  @param  meteorites  array of objects to store
     */
    func storeMeteoritesInBackground(meteorites: [Meteorite]) {
        
        // perform this task in background thread
        DispatchQueue.global().async {
            let realm = try! Realm()
            
            realm.beginWrite()
            
            // clean table
            let allMeteorites = realm.objects(Meteorite.self)
            realm.delete(allMeteorites)
            
            // write new data
            realm.add(meteorites)
            try! realm.commitWrite()
        }
    }
    
    /**
     *  Helping method for printing all object in Meteorite table
     */
    func printMeteorites() {
        let result = self.realm.objects(Meteorite.self)
        print("Printing all meteorites in table:")
        print(result)
    }
        
}
