//
//  HomeTableViewController.swift
//  Meteoric
//
//  Created by Jan Švancer on 06.11.17.
//  Copyright © 2017 Jan Švancer. All rights reserved.
//

import UIKit
import RealmSwift

class HomeTableViewController: UITableViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var meteoritesCountLabel: UILabel!
    @IBOutlet weak var landedLabel: UILabel!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    
    let kHeaderViewHeight: CGFloat = 150.0
    
    let dbResult = try! Realm().objects(Meteorite.self).sorted(byKeyPath: "mass", ascending: false)
    var notificationToken: NotificationToken?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // observe realm db changes
        self.notificationToken = self.dbResult.observe({ (changes: RealmCollectionChange) in
            switch changes {
            case .initial:
                fallthrough
            case .update(_, deletions: _, insertions: _, modifications: _):
                self.tableView.reloadData()
                self.setupHeaderView()
            case .error(let error):
                print("Error: \(error)")
            }
        })
        
        // setup table view cell separator
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.tableView.separatorColor = UIColor.white
        self.tableView.tableFooterView = UIView()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dbResult.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "meteoriteCell", for: indexPath) as! MeteoriteTableViewCell

        cell.configure(meteorite: self.dbResult[indexPath.row])

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    // MARK: UISCrollViewDelegate
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offsetY = scrollView.contentOffset.y + view.safeAreaInsets.top
        
        if offsetY < 0 {
            self.headerView.frame.origin.y = offsetY
            self.headerView.frame.size.height = -offsetY + self.kHeaderViewHeight
        }
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goto_detail" {
            if let detailVC = segue.destination as? DetailViewController,
                let sender = sender as? UITableViewCell,
                let selectedIndex = self.tableView.indexPath(for: sender) {
                detailVC.meteorite = self.dbResult[selectedIndex.row]
            }
        }
    }
    
    // MARK: Header view
    
    func setupHeaderView() {
        
        if self.dbResult.count > 0 {
            // data downloaded
            self.meteoritesCountLabel.text = String(self.dbResult.count)
            self.landedLabel.text = "Meteorites landed since \(APIService.shared.startYear)"
            
            if let lastSync = UserDefaults.standard.object(forKey: "lastSync") as? Date {
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .short
                dateFormatter.timeStyle = .medium
                self.lastUpdateLabel.text = "Last sync: \(dateFormatter.string(from: lastSync))"
            } else {
                self.lastUpdateLabel.text = "Last sync: ----"
            }
        } else {
            // no data
            self.meteoritesCountLabel.text = "No data"
            self.landedLabel.text = "Check your internet connection"
            self.lastUpdateLabel.text = "Last sync: ----"
        }
    }

}
