//
//  MeteoriteTableViewCell.swift
//  Meteoric
//
//  Created by Jan Švancer on 07.11.17.
//  Copyright © 2017 Jan Švancer. All rights reserved.
//

import UIKit
import MapKit

class MeteoriteTableViewCell: UITableViewCell {

    @IBOutlet weak var mapImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var massLabel: UILabel!
    @IBOutlet weak var recclassLabel: UILabel!
    
    let dateFormatter = DateFormatter()
    
    /**
     *  Configures cell with meteorite object
     *
     *  @param  meteorite   meteorite object from DB
     */
    func configure(meteorite: Meteorite) {
        
        if meteorite.placeAvailable {
            self.createMapImage(reclat: meteorite.reclat, reclong: meteorite.reclong)
        } else {
            self.mapImage.image = #imageLiteral(resourceName: "NAicon")
        }
        
        self.nameLabel.text = meteorite.name
        
        if let year = meteorite.year {
            self.dateFormatter.dateFormat = "YYYY"
            self.yearLabel.text = self.dateFormatter.string(from: year)
        } else {
            self.yearLabel.text = "----"
        }
        
        self.massLabel.text = "\(meteorite.mass)g"
        self.recclassLabel.text = meteorite.recclass
    }
    
    /**
     *  Creates image from map with location
     *
     *  @param  reclat  latitude
     *  @param  reclong longitude
     */
    func createMapImage(reclat: Double, reclong: Double) {
        let mapSnapshotOptions = MKMapSnapshotOptions()
        
        let location = CLLocation(latitude: reclat, longitude: reclong)
        let regionRadius: CLLocationDistance = 1000000
        let region = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        mapSnapshotOptions.region = region
        mapSnapshotOptions.scale = UIScreen.main.scale
        mapSnapshotOptions.size = CGSize(width: self.mapImage.bounds.size.width, height: self.mapImage.bounds.size.height)
        mapSnapshotOptions.showsBuildings = true
        mapSnapshotOptions.showsPointsOfInterest = true
        let snapShotter = MKMapSnapshotter(options: mapSnapshotOptions)
        
        snapShotter.start { (snapshot, error) in
            guard let snapshot = snapshot else {
                return
            }
            
            self.mapImage.image = snapshot.image
        }
    }

}
