//
//  DetailViewController.swift
//  Meteoric
//
//  Created by Jan Švancer on 06.11.17.
//  Copyright © 2017 Jan Švancer. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var placeNAView: UIView!
    @IBOutlet weak var idValueLabel: UILabel!
    @IBOutlet weak var nametypeValueLabel: UILabel!
    @IBOutlet weak var recclassValueLabel: UILabel!
    @IBOutlet weak var massValueLabel: UILabel!
    @IBOutlet weak var fallValueLabel: UILabel!
    @IBOutlet weak var yearValueLabel: UILabel!
    @IBOutlet weak var locationValueLabel: UILabel!
    
    var meteorite: Meteorite!
    let dateFormatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = self.meteorite.name
        
        // custom back bar button
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "leftArrow"), style: .plain, target: self, action: #selector(backButtonPressed(sender:)))
        self.navigationItem.leftBarButtonItem = backButton
        
        // show map when place is available, otherwise show NA view placeholder
        self.mapView.isHidden = !self.meteorite.placeAvailable
        self.placeNAView.isHidden = self.meteorite.placeAvailable
        
        if self.meteorite.placeAvailable {
            self.setupMap()
        }
        self.setupLabels()
    }
    
    @objc func backButtonPressed(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

    func setupMap() {
        
        // setup map location and region
        let location = CLLocation(latitude: self.meteorite.reclat, longitude: self.meteorite.reclong)
        let regionRadius: CLLocationDistance = 100000
        let region = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        
        self.mapView.setRegion(region, animated: true)
        
        // create and add annotation to map
        let annotation = MKPointAnnotation()
        annotation.coordinate = location.coordinate
        self.mapView.addAnnotation(annotation)
    }
    
    func setupLabels() {
        self.idValueLabel.text = String(self.meteorite.id)
        self.nametypeValueLabel.text = self.meteorite.nametype
        self.recclassValueLabel.text = self.meteorite.recclass
        self.massValueLabel.text = "\(self.meteorite.mass)g"
        self.fallValueLabel.text = self.meteorite.fall
        
        if let year = self.meteorite.year {
            self.dateFormatter.dateFormat = "YYYY"
            self.yearValueLabel.text = self.dateFormatter.string(from: year)
        } else {
            self.yearValueLabel.text = "----"
        }
        
        self.locationValueLabel.text = "(\(self.meteorite.reclat)°, \(self.meteorite.reclong)°)"
    }
}
