//
//  Meteorite.swift
//  Meteoric
//
//  Created by Jan Švancer on 06.11.17.
//  Copyright © 2017 Jan Švancer. All rights reserved.
//

import UIKit
import RealmSwift

class Meteorite: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var nametype: String = ""
    @objc dynamic var fall: String = ""
    @objc dynamic var mass: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var recclass: String = ""
    @objc dynamic var reclat: Double = 0.0
    @objc dynamic var reclong: Double = 0.0
    @objc dynamic var year: Date?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // if we have geolocation the place is known and available to display
    var placeAvailable: Bool {
        return self.reclat != 0.0 && self.reclong != 0.0
    }
}
